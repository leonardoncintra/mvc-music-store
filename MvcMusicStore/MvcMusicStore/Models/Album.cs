﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore.Models
{
    [Bind(Exclude="AlbumId")]
    public class Album
    {
        [ScaffoldColumn(false)]
        public int AlbumId { get; set; }
        public string Title { get; set; }
        
        [Display(Name="Album")]
        public int ArtistId { get; set; }
        
        [Display(Name="Gênero")]
        public int GenreId { get; set; }
        
        [Required(ErrorMessage="Preço é obrigatório"), Range(0.01, 100.00, ErrorMessage="Mínimo de R$ 0,00 e máximo de R$ 100,00")]
        public decimal Price { get; set; }
        public string AlbumArtUrl { get; set; }
        public virtual Genre Genre { get; set; }
        public virtual Artist Artist { get; set; }

    }
}